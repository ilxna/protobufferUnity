// Generated by ProtoGen, Version=0.9.0.0, Culture=neutral, PublicKeyToken=8fd7408b07f8d2cd.  DO NOT EDIT!

using pb = global::Google.ProtocolBuffers;
using pbc = global::Google.ProtocolBuffers.Collections;
using pbd = global::Google.ProtocolBuffers.Descriptors;
using scg = global::System.Collections.Generic;
namespace ChuMeng {
  
  public static partial class MHoroscopeProtoc {
  
    #region Extension registration
    public static void RegisterAllExtensions(pb::ExtensionRegistry registry) {
    }
    #endregion
    #region Static variables
    #endregion
    #region Extensions
    internal static readonly object Descriptor;
    static MHoroscopeProtoc() {
      Descriptor = null;
    }
    #endregion
    
  }
  #region Messages
  public sealed partial class CGLoadHoroscope : pb::GeneratedMessageLite<CGLoadHoroscope, CGLoadHoroscope.Builder> {
    private static readonly CGLoadHoroscope defaultInstance = new Builder().BuildPartial();
    public static CGLoadHoroscope DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override CGLoadHoroscope DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override CGLoadHoroscope ThisMessage {
      get { return this; }
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      CGLoadHoroscope other = obj as CGLoadHoroscope;
      if (other == null) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
    }
    #endregion
    
    public static CGLoadHoroscope ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static CGLoadHoroscope ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static CGLoadHoroscope ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGLoadHoroscope ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(CGLoadHoroscope prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<CGLoadHoroscope, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      CGLoadHoroscope result = new CGLoadHoroscope();
      
      protected override CGLoadHoroscope MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new CGLoadHoroscope();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override CGLoadHoroscope DefaultInstanceForType {
        get { return global::ChuMeng.CGLoadHoroscope.DefaultInstance; }
      }
      
      public override CGLoadHoroscope BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        CGLoadHoroscope returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is CGLoadHoroscope) {
          return MergeFrom((CGLoadHoroscope) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(CGLoadHoroscope other) {
        if (other == global::ChuMeng.CGLoadHoroscope.DefaultInstance) return this;
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
          }
        }
      }
      
    }
    static CGLoadHoroscope() {
      object.ReferenceEquals(global::ChuMeng.MHoroscopeProtoc.Descriptor, null);
    }
  }
  
  public sealed partial class GCLoadHoroscope : pb::GeneratedMessageLite<GCLoadHoroscope, GCLoadHoroscope.Builder> {
    private static readonly GCLoadHoroscope defaultInstance = new Builder().BuildPartial();
    public static GCLoadHoroscope DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override GCLoadHoroscope DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override GCLoadHoroscope ThisMessage {
      get { return this; }
    }
    
    public const int HoroscopeIdFieldNumber = 1;
    private bool hasHoroscopeId;
    private int horoscopeId_ = 0;
    public bool HasHoroscopeId {
      get { return hasHoroscopeId; }
    }
    public int HoroscopeId {
      get { return horoscopeId_; }
      set { SethoroscopeId(value); }
    }
    private void SethoroscopeId(int value) {
      hasHoroscopeId = true;
      horoscopeId_ = value;
    }
    
    public override bool IsInitialized {
      get {
        if (!hasHoroscopeId) return false;
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
      if (HasHoroscopeId) {
        output.WriteInt32(1, HoroscopeId);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (HasHoroscopeId) {
          size += pb::CodedOutputStream.ComputeInt32Size(1, HoroscopeId);
        }
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasHoroscopeId) hash ^= horoscopeId_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      GCLoadHoroscope other = obj as GCLoadHoroscope;
      if (other == null) return false;
      if (hasHoroscopeId != other.hasHoroscopeId || (hasHoroscopeId && !horoscopeId_.Equals(other.horoscopeId_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("horoscopeId", hasHoroscopeId, horoscopeId_, writer);
    }
    #endregion
    
    public static GCLoadHoroscope ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static GCLoadHoroscope ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static GCLoadHoroscope ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCLoadHoroscope ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(GCLoadHoroscope prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<GCLoadHoroscope, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      GCLoadHoroscope result = new GCLoadHoroscope();
      
      protected override GCLoadHoroscope MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new GCLoadHoroscope();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override GCLoadHoroscope DefaultInstanceForType {
        get { return global::ChuMeng.GCLoadHoroscope.DefaultInstance; }
      }
      
      public override GCLoadHoroscope BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        GCLoadHoroscope returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is GCLoadHoroscope) {
          return MergeFrom((GCLoadHoroscope) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(GCLoadHoroscope other) {
        if (other == global::ChuMeng.GCLoadHoroscope.DefaultInstance) return this;
        if (other.HasHoroscopeId) {
          HoroscopeId = other.HoroscopeId;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
            case 8: {
              HoroscopeId = input.ReadInt32();
              break;
            }
          }
        }
      }
      
      
      public bool HasHoroscopeId {
        get { return result.HasHoroscopeId; }
      }
      public int HoroscopeId {
        get { return result.HoroscopeId; }
        set { SetHoroscopeId(value); }
      }
      public Builder SetHoroscopeId(int value) {
        result.hasHoroscopeId = true;
        result.horoscopeId_ = value;
        return this;
      }
      public Builder ClearHoroscopeId() {
        result.hasHoroscopeId = false;
        result.horoscopeId_ = 0;
        return this;
      }
    }
    static GCLoadHoroscope() {
      object.ReferenceEquals(global::ChuMeng.MHoroscopeProtoc.Descriptor, null);
    }
  }
  
  public sealed partial class CGActHoroscope : pb::GeneratedMessageLite<CGActHoroscope, CGActHoroscope.Builder> {
    private static readonly CGActHoroscope defaultInstance = new Builder().BuildPartial();
    public static CGActHoroscope DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override CGActHoroscope DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override CGActHoroscope ThisMessage {
      get { return this; }
    }
    
    public const int HoroscopeIdFieldNumber = 1;
    private bool hasHoroscopeId;
    private int horoscopeId_ = 0;
    public bool HasHoroscopeId {
      get { return hasHoroscopeId; }
    }
    public int HoroscopeId {
      get { return horoscopeId_; }
      set { SethoroscopeId(value); }
    }
    private void SethoroscopeId(int value) {
      hasHoroscopeId = true;
      horoscopeId_ = value;
    }
    
    public override bool IsInitialized {
      get {
        if (!hasHoroscopeId) return false;
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
      if (HasHoroscopeId) {
        output.WriteInt32(1, HoroscopeId);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (HasHoroscopeId) {
          size += pb::CodedOutputStream.ComputeInt32Size(1, HoroscopeId);
        }
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasHoroscopeId) hash ^= horoscopeId_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      CGActHoroscope other = obj as CGActHoroscope;
      if (other == null) return false;
      if (hasHoroscopeId != other.hasHoroscopeId || (hasHoroscopeId && !horoscopeId_.Equals(other.horoscopeId_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("horoscopeId", hasHoroscopeId, horoscopeId_, writer);
    }
    #endregion
    
    public static CGActHoroscope ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static CGActHoroscope ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static CGActHoroscope ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGActHoroscope ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(CGActHoroscope prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<CGActHoroscope, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      CGActHoroscope result = new CGActHoroscope();
      
      protected override CGActHoroscope MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new CGActHoroscope();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override CGActHoroscope DefaultInstanceForType {
        get { return global::ChuMeng.CGActHoroscope.DefaultInstance; }
      }
      
      public override CGActHoroscope BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        CGActHoroscope returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is CGActHoroscope) {
          return MergeFrom((CGActHoroscope) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(CGActHoroscope other) {
        if (other == global::ChuMeng.CGActHoroscope.DefaultInstance) return this;
        if (other.HasHoroscopeId) {
          HoroscopeId = other.HoroscopeId;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
            case 8: {
              HoroscopeId = input.ReadInt32();
              break;
            }
          }
        }
      }
      
      
      public bool HasHoroscopeId {
        get { return result.HasHoroscopeId; }
      }
      public int HoroscopeId {
        get { return result.HoroscopeId; }
        set { SetHoroscopeId(value); }
      }
      public Builder SetHoroscopeId(int value) {
        result.hasHoroscopeId = true;
        result.horoscopeId_ = value;
        return this;
      }
      public Builder ClearHoroscopeId() {
        result.hasHoroscopeId = false;
        result.horoscopeId_ = 0;
        return this;
      }
    }
    static CGActHoroscope() {
      object.ReferenceEquals(global::ChuMeng.MHoroscopeProtoc.Descriptor, null);
    }
  }
  
  public sealed partial class GCActHoroscope : pb::GeneratedMessageLite<GCActHoroscope, GCActHoroscope.Builder> {
    private static readonly GCActHoroscope defaultInstance = new Builder().BuildPartial();
    public static GCActHoroscope DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override GCActHoroscope DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override GCActHoroscope ThisMessage {
      get { return this; }
    }
    
    public const int SuccessFieldNumber = 1;
    private bool hasSuccess;
    private bool success_ = false;
    public bool HasSuccess {
      get { return hasSuccess; }
    }
    public bool Success {
      get { return success_; }
      set { Setsuccess(value); }
    }
    private void Setsuccess(bool value) {
      hasSuccess = true;
      success_ = value;
    }
    
    public override bool IsInitialized {
      get {
        if (!hasSuccess) return false;
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
      if (HasSuccess) {
        output.WriteBool(1, Success);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (HasSuccess) {
          size += pb::CodedOutputStream.ComputeBoolSize(1, Success);
        }
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasSuccess) hash ^= success_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      GCActHoroscope other = obj as GCActHoroscope;
      if (other == null) return false;
      if (hasSuccess != other.hasSuccess || (hasSuccess && !success_.Equals(other.success_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("success", hasSuccess, success_, writer);
    }
    #endregion
    
    public static GCActHoroscope ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static GCActHoroscope ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static GCActHoroscope ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCActHoroscope ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(GCActHoroscope prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<GCActHoroscope, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      GCActHoroscope result = new GCActHoroscope();
      
      protected override GCActHoroscope MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new GCActHoroscope();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override GCActHoroscope DefaultInstanceForType {
        get { return global::ChuMeng.GCActHoroscope.DefaultInstance; }
      }
      
      public override GCActHoroscope BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        GCActHoroscope returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is GCActHoroscope) {
          return MergeFrom((GCActHoroscope) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(GCActHoroscope other) {
        if (other == global::ChuMeng.GCActHoroscope.DefaultInstance) return this;
        if (other.HasSuccess) {
          Success = other.Success;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
            case 8: {
              Success = input.ReadBool();
              break;
            }
          }
        }
      }
      
      
      public bool HasSuccess {
        get { return result.HasSuccess; }
      }
      public bool Success {
        get { return result.Success; }
        set { SetSuccess(value); }
      }
      public Builder SetSuccess(bool value) {
        result.hasSuccess = true;
        result.success_ = value;
        return this;
      }
      public Builder ClearSuccess() {
        result.hasSuccess = false;
        result.success_ = false;
        return this;
      }
    }
    static GCActHoroscope() {
      object.ReferenceEquals(global::ChuMeng.MHoroscopeProtoc.Descriptor, null);
    }
  }
  
  #endregion
  
}
