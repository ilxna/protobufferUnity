# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: MCopyProtoc.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import CommonProtoc_pb2
import google.protobuf.csharp_options_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='MCopyProtoc.proto',
  package='protocmsg',
  serialized_pb='\n\x11MCopyProtoc.proto\x12\tprotocmsg\x1a\x12\x43ommonProtoc.proto\x1a$google/protobuf/csharp_options.proto\"\x0c\n\nCGCopyInfo\"3\n\nGCCopyInfo\x12%\n\x08\x63opyInfo\x18\x01 \x03(\x0b\x32\x13.protocmsg.CopyInfo\"&\n\x08\x43opyInfo\x12\n\n\x02id\x18\x01 \x01(\x05\x12\x0e\n\x06isPass\x18\x02 \x01(\x08\"^\n\x0c\x43GCopyReward\x12\x14\n\x0csilverTicket\x18\x01 \x01(\x05\x12\x0f\n\x07\x65xpGain\x18\x02 \x01(\x05\x12\'\n\tpackEntry\x18\x03 \x03(\x0b\x32\x14.protocmsg.PackEntry\"\x0e\n\x0cGCCopyReward\"1\n\x0fGCPushLevelOpen\x12\x0f\n\x07\x63hapter\x18\x01 \x01(\x05\x12\r\n\x05level\x18\x02 \x01(\x05\x42I\n\x1c\x63om.td.starfantasy.protocmsgB\x0bMCopyProtocH\x03\xc2>\t\n\x07\x43huMeng\xc2>\r\x12\x0bMCopyProtoc')




_CGCOPYINFO = _descriptor.Descriptor(
  name='CGCopyInfo',
  full_name='protocmsg.CGCopyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=90,
  serialized_end=102,
)


_GCCOPYINFO = _descriptor.Descriptor(
  name='GCCopyInfo',
  full_name='protocmsg.GCCopyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='copyInfo', full_name='protocmsg.GCCopyInfo.copyInfo', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=104,
  serialized_end=155,
)


_COPYINFO = _descriptor.Descriptor(
  name='CopyInfo',
  full_name='protocmsg.CopyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocmsg.CopyInfo.id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='isPass', full_name='protocmsg.CopyInfo.isPass', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=157,
  serialized_end=195,
)


_CGCOPYREWARD = _descriptor.Descriptor(
  name='CGCopyReward',
  full_name='protocmsg.CGCopyReward',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='silverTicket', full_name='protocmsg.CGCopyReward.silverTicket', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='expGain', full_name='protocmsg.CGCopyReward.expGain', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='packEntry', full_name='protocmsg.CGCopyReward.packEntry', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=197,
  serialized_end=291,
)


_GCCOPYREWARD = _descriptor.Descriptor(
  name='GCCopyReward',
  full_name='protocmsg.GCCopyReward',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=293,
  serialized_end=307,
)


_GCPUSHLEVELOPEN = _descriptor.Descriptor(
  name='GCPushLevelOpen',
  full_name='protocmsg.GCPushLevelOpen',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='chapter', full_name='protocmsg.GCPushLevelOpen.chapter', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='level', full_name='protocmsg.GCPushLevelOpen.level', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=309,
  serialized_end=358,
)

_GCCOPYINFO.fields_by_name['copyInfo'].message_type = _COPYINFO
_CGCOPYREWARD.fields_by_name['packEntry'].message_type = CommonProtoc_pb2._PACKENTRY
DESCRIPTOR.message_types_by_name['CGCopyInfo'] = _CGCOPYINFO
DESCRIPTOR.message_types_by_name['GCCopyInfo'] = _GCCOPYINFO
DESCRIPTOR.message_types_by_name['CopyInfo'] = _COPYINFO
DESCRIPTOR.message_types_by_name['CGCopyReward'] = _CGCOPYREWARD
DESCRIPTOR.message_types_by_name['GCCopyReward'] = _GCCOPYREWARD
DESCRIPTOR.message_types_by_name['GCPushLevelOpen'] = _GCPUSHLEVELOPEN

class CGCopyInfo(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CGCOPYINFO

  # @@protoc_insertion_point(class_scope:protocmsg.CGCopyInfo)

class GCCopyInfo(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCCOPYINFO

  # @@protoc_insertion_point(class_scope:protocmsg.GCCopyInfo)

class CopyInfo(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _COPYINFO

  # @@protoc_insertion_point(class_scope:protocmsg.CopyInfo)

class CGCopyReward(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CGCOPYREWARD

  # @@protoc_insertion_point(class_scope:protocmsg.CGCopyReward)

class GCCopyReward(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCCOPYREWARD

  # @@protoc_insertion_point(class_scope:protocmsg.GCCopyReward)

class GCPushLevelOpen(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCPUSHLEVELOPEN

  # @@protoc_insertion_point(class_scope:protocmsg.GCPushLevelOpen)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), '\n\034com.td.starfantasy.protocmsgB\013MCopyProtocH\003\302>\t\n\007ChuMeng\302>\r\022\013MCopyProtoc')
# @@protoc_insertion_point(module_scope)
