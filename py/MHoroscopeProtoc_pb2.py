# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: MHoroscopeProtoc.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import google.protobuf.csharp_options_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='MHoroscopeProtoc.proto',
  package='protocmsg',
  serialized_pb='\n\x16MHoroscopeProtoc.proto\x12\tprotocmsg\x1a$google/protobuf/csharp_options.proto\"\x11\n\x0f\x43GLoadHoroscope\"&\n\x0fGCLoadHoroscope\x12\x13\n\x0bhoroscopeId\x18\x01 \x02(\x05\"%\n\x0e\x43GActHoroscope\x12\x13\n\x0bhoroscopeId\x18\x01 \x02(\x05\"!\n\x0eGCActHoroscope\x12\x0f\n\x07success\x18\x01 \x02(\x08\x42S\n\x1c\x63om.td.starfantasy.protocmsgB\x10MHoroscopeProtocH\x03\xc2>\t\n\x07\x43huMeng\xc2>\x12\x12\x10MHoroscopeProtoc')




_CGLOADHOROSCOPE = _descriptor.Descriptor(
  name='CGLoadHoroscope',
  full_name='protocmsg.CGLoadHoroscope',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=75,
  serialized_end=92,
)


_GCLOADHOROSCOPE = _descriptor.Descriptor(
  name='GCLoadHoroscope',
  full_name='protocmsg.GCLoadHoroscope',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='horoscopeId', full_name='protocmsg.GCLoadHoroscope.horoscopeId', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=94,
  serialized_end=132,
)


_CGACTHOROSCOPE = _descriptor.Descriptor(
  name='CGActHoroscope',
  full_name='protocmsg.CGActHoroscope',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='horoscopeId', full_name='protocmsg.CGActHoroscope.horoscopeId', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=134,
  serialized_end=171,
)


_GCACTHOROSCOPE = _descriptor.Descriptor(
  name='GCActHoroscope',
  full_name='protocmsg.GCActHoroscope',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='success', full_name='protocmsg.GCActHoroscope.success', index=0,
      number=1, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=173,
  serialized_end=206,
)

DESCRIPTOR.message_types_by_name['CGLoadHoroscope'] = _CGLOADHOROSCOPE
DESCRIPTOR.message_types_by_name['GCLoadHoroscope'] = _GCLOADHOROSCOPE
DESCRIPTOR.message_types_by_name['CGActHoroscope'] = _CGACTHOROSCOPE
DESCRIPTOR.message_types_by_name['GCActHoroscope'] = _GCACTHOROSCOPE

class CGLoadHoroscope(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CGLOADHOROSCOPE

  # @@protoc_insertion_point(class_scope:protocmsg.CGLoadHoroscope)

class GCLoadHoroscope(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCLOADHOROSCOPE

  # @@protoc_insertion_point(class_scope:protocmsg.GCLoadHoroscope)

class CGActHoroscope(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CGACTHOROSCOPE

  # @@protoc_insertion_point(class_scope:protocmsg.CGActHoroscope)

class GCActHoroscope(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCACTHOROSCOPE

  # @@protoc_insertion_point(class_scope:protocmsg.GCActHoroscope)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), '\n\034com.td.starfantasy.protocmsgB\020MHoroscopeProtocH\003\302>\t\n\007ChuMeng\302>\022\022\020MHoroscopeProtoc')
# @@protoc_insertion_point(module_scope)
